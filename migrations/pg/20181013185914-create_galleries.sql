-- +migrate Up
CREATE TABLE galleries(
  id SERIAL PRIMARY KEY,
  created_at TIMESTAMP NOT NULL,
  updated_at TIMESTAMP NOT NULL,
  deleted_at TIMESTAMP,
  user_id INTEGER REFERENCES users (id) ON DELETE CASCADE,
  title VARCHAR UNIQUE NOT NULL, CHECK (title <> '')
);

-- +migrate Down
DROP TABLE galleries;