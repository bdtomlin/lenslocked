
-- +migrate Up
ALTER TABLE users
ADD COLUMN remember_hash VARCHAR NOT NULL;

CREATE UNIQUE INDEX ON users (remember_hash);

-- +migrate Down
ALTER TABLE users
DROP COLUMN remember_hash;
