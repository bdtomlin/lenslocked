-- +migrate Up
CREATE EXTENSION IF NOT EXISTS citext;
create table users (
  id SERIAL PRIMARY KEY,
  created_at TIMESTAMP NOT NULL,
  updated_at TIMESTAMP NOT NULL,
  deleted_at TIMESTAMP,
  name TEXT,
  email CITEXT UNIQUE NOT NULL
);


-- +migrate Down
DROP TABLE users;


