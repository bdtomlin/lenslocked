-- +migrate Up
ALTER TABLE users
ADD COLUMN password_hash VARCHAR NOT NULL;


-- +migrate Down
ALTER TABLE users
DROP COLUMN password_hash;
