
-- +migrate Up
CREATE TABLE orders (
  id SERIAL PRIMARY KEY,
  created_at TIMESTAMP NOT NULL,
  updated_at TIMESTAMP NOT NULL,
  deleted_at TIMESTAMP,
  user_id INTEGER REFERENCES users (id) ON DELETE CASCADE,
  amount INTEGER NOT NULL DEFAULT 0,
  description VARCHAR
);

-- +migrate Down
DROP TABLE orders;
