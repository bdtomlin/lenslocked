package main

import (
	"github.com/gorilla/handlers"
	"lenslocked.com/models"
	"lenslocked.com/router"
	"net/http"
)

func main() {
	services, err := models.NewServices()
	if err != nil {
		panic(err)
	}
	defer services.Close()

	r := router.NewRouter(services)

	err = http.ListenAndServe(":3000", handlers.RecoveryHandler()(r))
	if err != nil {
		panic(err)
	}
}
