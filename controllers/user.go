package controllers

import (
	"fmt"
	"lenslocked.com/models"
	"lenslocked.com/rand"
	"lenslocked.com/views"
	"log"
	"net/http"
	"time"
)

type UserController struct {
	NewView   *views.View
	LoginView *views.View
	us        models.UserService
}

type SignupForm struct {
	Name     string `schema:"name"`
	Email    string `schema:"email"`
	Password string `schema:"password"`
}

type LoginForm struct {
	Email    string `schema:"email"`
	Password string `schema:"password"`
}

func NewUserController(us models.UserService) *UserController {
	return &UserController{
		NewView:   views.NewView("bootstrap", "user/new"),
		LoginView: views.NewView("bootstrap", "user/login"),
		us:        us,
	}
}

func (uc *UserController) New(w http.ResponseWriter, r *http.Request) {
	uc.NewView.Render(w, r, nil)
}

func (uc *UserController) Create(w http.ResponseWriter, r *http.Request) {
	var vd views.Data
	var form SignupForm
	if err := parseForm(r, &form); err != nil {
		log.Println(err)
		vd.SetAlert(err)
		uc.NewView.Render(w, r, vd)
		return
	}

	user := models.User{
		Name:     form.Name,
		Email:    form.Email,
		Password: form.Password,
	}
	if err := uc.us.Create(&user); err != nil {
		vd.SetAlert(err)
		uc.NewView.Render(w, r, vd)
		return
	}

	err := uc.signIn(w, &user)
	if err != nil {
		http.Redirect(w, r, "/login", http.StatusNotFound)
		return
	}
	http.Redirect(w, r, "/galleries", http.StatusFound)
}

func (uc *UserController) GetLogin(w http.ResponseWriter, r *http.Request) {
	uc.LoginView.Render(w, r, nil)
}

func (uc *UserController) PostLogin(w http.ResponseWriter, r *http.Request) {
	var vd views.Data
	form := LoginForm{}
	if err := parseForm(r, &form); err != nil {
		vd.SetAlert(err)
		uc.LoginView.Render(w, r, vd)
		return
	}

	user, err := uc.us.Authenticate(form.Email, form.Password)
	if err != nil {
		switch err {
		case models.ErrNotFound:
			vd.AlertError("No user exists with that email address")
		default:
			vd.SetAlert(err)
		}
		uc.LoginView.Render(w, r, vd)
		return
	}

	err = uc.signIn(w, user)
	if err != nil {
		vd.SetAlert(err)
		uc.LoginView.Render(w, r, vd)
		return
	}
	http.Redirect(w, r, "/cookietest", http.StatusFound)
}

func (uc *UserController) CookieTest(w http.ResponseWriter, r *http.Request) {
	cookie, err := r.Cookie("remember_token")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	user, err := uc.us.ByRemember(cookie.Value)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	fmt.Fprintf(w, "%+v\n", user)
}

func (uc *UserController) signIn(w http.ResponseWriter, user *models.User) error {
	token, err := rand.RememberToken()
	if err != nil {
		return err
	}
	user.Remember = token
	err = uc.us.Update(user)
	if err != nil {
		return err
	}

	expiration := time.Now().Add(365 * 24 * time.Hour)
	cookie := http.Cookie{
		Name:     "remember_token",
		Value:    user.Remember,
		Expires:  expiration,
		HttpOnly: true,
	}
	http.SetCookie(w, &cookie)
	return nil
}
