package controllers

import (
	"fmt"
	"github.com/gorilla/mux"
	"lenslocked.com/context"
	"lenslocked.com/models"
	"lenslocked.com/views"
	"net/http"
	"strconv"
)

const (
	IndexGalleries  = "index_galleries"
	ShowGallery     = "show_gallery"
	EditGallery     = "edit_gallery"
	maxMultipartMem = 1 << 20
)

type GalleryController struct {
	NewView   *views.View
	ShowView  *views.View
	EditView  *views.View
	IndexView *views.View
	gs        models.GalleryService
	is        models.ImageService
	r         *mux.Router
}

type GalleryForm struct {
	Title *string `schema:"title"`
}

func NewGalleryController(gs models.GalleryService, is models.ImageService, r *mux.Router) *GalleryController {
	return &GalleryController{
		NewView:   views.NewView("bootstrap", "gallery/new"),
		ShowView:  views.NewView("bootstrap", "gallery/show"),
		EditView:  views.NewView("bootstrap", "gallery/edit"),
		IndexView: views.NewView("bootstrap", "gallery/index"),
		gs:        gs,
		is:        is,
		r:         r,
	}
}

func (gc *GalleryController) New(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html")
	gc.NewView.Render(w, r, nil)
}

func (gc *GalleryController) Index(w http.ResponseWriter, r *http.Request) {
	user := context.User(r.Context())
	galleries, err := gc.gs.ByUserID(user.ID)
	if err != nil {
		http.Error(w, "Something went wrong.", http.StatusInternalServerError)
		return
	}
	var vd views.Data
	vd.Yield = galleries
	gc.IndexView.Render(w, r, vd)
}

func (gc *GalleryController) Show(w http.ResponseWriter, r *http.Request) {
	gallery, err := gc.galleryByID(w, r)
	if err != nil {
		return
	}
	var vd views.Data
	vd.Yield = gallery
	gc.ShowView.Render(w, r, vd)
}

func (gc *GalleryController) Create(w http.ResponseWriter, r *http.Request) {
	var vd views.Data
	var form GalleryForm
	if err := parseForm(r, &form); err != nil {
		vd.SetAlert(err)
		gc.NewView.Render(w, r, vd)
		return
	}

	user := context.User(r.Context())
	gallery := models.Gallery{
		Title:  form.Title,
		UserID: user.ID,
	}

	if err := gc.gs.Create(&gallery); err != nil {
		vd.SetAlert(err)
		gc.NewView.Render(w, r, vd)
		return
	}
	url, err := gc.r.Get(EditGallery).URL("id", strconv.Itoa(int(gallery.ID)))
	if err != nil {
		http.Redirect(w, r, "/", http.StatusFound)
		return
	}
	http.Redirect(w, r, url.Path, http.StatusFound)
}

func (gc *GalleryController) Edit(w http.ResponseWriter, r *http.Request) {
	gallery, err := gc.galleryByID(w, r)
	if err != nil {
		return
	}

	user := context.User(r.Context())
	if gallery.UserID != user.ID {
		http.Error(w, "You do not have permission to edit this gallery", http.StatusForbidden)
		return
	}

	var vd views.Data
	vd.Yield = gallery
	gc.EditView.Render(w, r, vd)
}

func (gc *GalleryController) Update(w http.ResponseWriter, r *http.Request) {
	gallery, err := gc.galleryByID(w, r)
	if err != nil {
		return
	}
	user := context.User(r.Context())
	if gallery.UserID != user.ID {
		http.Error(w, "Gallery not found", http.StatusNotFound)
		return
	}

	var vd views.Data
	vd.Yield = gallery
	var form GalleryForm
	if err := parseForm(r, &form); err != nil {
		vd.SetAlert(err)
		gc.EditView.Render(w, r, vd)
		return
	}
	gallery.Title = form.Title
	err = gc.gs.Update(gallery)
	if err != nil {
		vd.SetAlert(err)
	} else {
		vd.Alert = &views.Alert{
			Level:   views.AlertLevelSuccess,
			Message: "Gallery successfully updated!",
		}
	}

	gc.EditView.Render(w, r, vd)
}

func (gc *GalleryController) Delete(w http.ResponseWriter, r *http.Request) {
	gallery, err := gc.galleryByID(w, r)
	if err != nil {
		return
	}
	user := context.User(r.Context())
	if gallery.UserID != user.ID {
		http.Error(w, "Gallery not found", http.StatusNotFound)
		return
	}

	var vd views.Data
	err = gc.gs.Delete(gallery)
	if err != nil {
		vd.SetAlert(err)
		vd.Yield = gallery
		gc.EditView.Render(w, r, vd)
		return
	}

	url, err := gc.r.Get(IndexGalleries).URL()
	if err != nil {
		http.Redirect(w, r, "/", http.StatusFound)
		return
	}
	http.Redirect(w, r, url.Path, http.StatusFound)
}

func (gc *GalleryController) ImageUpload(w http.ResponseWriter, r *http.Request) {
	gallery, err := gc.galleryByID(w, r)
	if err != nil {
		return
	}
	user := context.User(r.Context())
	if gallery.UserID != user.ID {
		http.Error(w, "Gallery not found", http.StatusNotFound)
		return
	}

	var vd views.Data
	vd.Yield = gallery
	err = r.ParseMultipartForm(maxMultipartMem)
	if err != nil {
		vd.SetAlert(err)
		gc.EditView.Render(w, r, vd)
		return
	}

	files := r.MultipartForm.File["images"]
	for _, f := range files {
		file, err := f.Open()
		if err != nil {
			vd.SetAlert(err)
			gc.EditView.Render(w, r, vd)
			return
		}
		defer file.Close()

		err = gc.is.Create(gallery.ID, file, f.Filename)
		if err != nil {
			vd.SetAlert(err)
			gc.EditView.Render(w, r, vd)
			return
		}
	}

	vd.Alert = &views.Alert{
		Level:   views.AlertLevelSuccess,
		Message: "Images sucessfully uploaded!",
	}
	gc.EditView.Render(w, r, vd)
}

func (gc GalleryController) ImageDelete(w http.ResponseWriter, r *http.Request) {
	gallery, err := gc.galleryByID(w, r)
	if err != nil {
		return
	}
	user := context.User(r.Context())
	if gallery.UserID != user.ID {
		http.Error(w, "You don't have permission to edit this gallery or image", http.StatusForbidden)
		return
	}

	filename := mux.Vars(r)["filename"]
	i := models.Image{
		Filename:  filename,
		GalleryID: gallery.ID,
	}

	err = gc.is.Delete(&i)
	if err != nil {
		var vd views.Data
		vd.Yield = gallery
		vd.SetAlert(err)
		gc.EditView.Render(w, r, vd)
		return
	}

	url, err := gc.r.Get(EditGallery).URL("id", fmt.Sprintf("%v", gallery.ID))
	if err != nil {
		http.Redirect(w, r, "/galleries", http.StatusNotFound)
	}
	http.Redirect(w, r, url.Path, http.StatusFound)
}

func (gc *GalleryController) galleryByID(w http.ResponseWriter, r *http.Request) (*models.Gallery, error) {
	vars := mux.Vars(r)
	idStr := vars["id"]
	id, err := strconv.Atoi(idStr)
	if err != nil {
		http.Error(w, "Invalid gallery ID", http.StatusNotFound)
		return nil, err
	}
	gallery, err := gc.gs.ByID(uint(id))
	if err != nil {
		switch err {
		case models.ErrNotFound:
			http.Error(w, "Gallery not found", http.StatusNotFound)
		default:
			http.Error(w, "Whoops! Something went wrong.", http.StatusInternalServerError)
		}
		return nil, err
	}
	images, _ := gc.is.ByGalleryID(gallery.ID)
	gallery.Images = images
	return gallery, nil
}
