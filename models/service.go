package models

import "github.com/jinzhu/gorm"

type Services struct {
	Gallery GalleryService
	User    UserService
	Image   ImageService
	db      *gorm.DB
}

func NewServices() (*Services, error) {
	db, err := gorm.Open("postgres", "")
	if err != nil {
		return nil, err
	}
	db.LogMode(true)

	return &Services{
		User:    NewUserService(db),
		Gallery: NewGalleryService(db),
		Image:   NewImageService(),
		db:      db,
	}, nil
}

func (s *Services) Close() error {
	return s.db.Close()
}

func (s *Services) DestructiveReset() error {
	s.db.Exec("TRUNCATE users cascade;")
	s.db.Exec("ALTER SEQUENCE users_id_seq RESTART;")
	s.db.Exec("TRUNCATE galleries cascade;")
	s.db.Exec("ALTER SEQUENCE galleries_id_seq RESTART;")
	return s.db.Error
}
