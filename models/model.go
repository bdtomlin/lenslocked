package models

import "errors"

var (
	ErrNotFound  = errors.New("models: resource not found")
	ErrIDInvalid = errors.New("models: ID provided was invalid")
)
