package views

import (
	"bytes"
	"fmt"
	"html/template"
	"io"
	"lenslocked.com/context"
	"net/http"
)

var (
	TemplateDir = "templates"
	LayoutDir   = "layouts"
	TemplateExt = "gohtml"
)

type View struct {
	Template *template.Template
	Layout   string
}

func NewView(layout string, files ...string) *View {
	t := template.New("")

	fullFilePaths(files)
	parseFiles(t, files)
	parseLayouts(t)

	return &View{
		Template: t,
		Layout:   layout,
	}
}

func (v *View) Render(w http.ResponseWriter, r *http.Request, data interface{}) {
	w.Header().Set("Content-Type", "text/html")
	var vd Data
	switch d := data.(type) {
	case Data:
		vd = d
	default:
		vd = Data{
			Yield: data,
		}
	}

	vd.User = context.User(r.Context())
	var buf bytes.Buffer
	var err error

	if v.Layout == "" {
		err = v.Template.Execute(&buf, vd)
	} else {
		err = v.Template.ExecuteTemplate(&buf, v.Layout, vd)
	}

	if err != nil {
		http.Error(w,
			"Something went wrong, if the problem persists, too bad.",
			http.StatusInternalServerError,
		)
		return
	}

	io.Copy(w, &buf)
}

func (v *View) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	v.Render(w, r, nil)
}

func parseFiles(t *template.Template, files []string) {
	t, err := t.ParseFiles(files...)
	if err != nil {
		panic(err)
	}
}

func fullFilePaths(files []string) {
	for i, f := range files {
		files[i] = fmt.Sprintf("%s/%s.%s", TemplateDir, f, TemplateExt)
	}
}

func parseLayouts(t *template.Template) {
	t, err := t.ParseGlob(fmt.Sprintf("%s/%s/*.%s", TemplateDir, LayoutDir, TemplateExt))
	if err != nil {
		panic(err)
	}
}
