package views

import (
	"lenslocked.com/models"
	"log"
)

const (
	AlertLevelError     = "danger"
	AlertLevelWarning   = "warning"
	AlertLevelInfo      = "info"
	AlertLevelSuccess   = "success"
	AlertMessageGeneric = "Something went wrong, Please try again and contact us if the problem persists."
)

type Data struct {
	Alert *Alert
	User  *models.User
	Yield interface{}
}

type Alert struct {
	Level   string
	Message string
}

type PublicError interface {
	error
	Public() string
}

func (d *Data) SetAlert(err error) {
	var msg string
	if pErr, ok := err.(PublicError); ok {
		msg = pErr.Public()
	} else {
		log.Println(err)
		msg = AlertMessageGeneric
	}
	d.Alert = &Alert{
		Level:   AlertLevelError,
		Message: msg,
	}
}

func (d *Data) AlertError(msg string) {
	d.Alert = &Alert{
		Level:   AlertLevelError,
		Message: msg,
	}
}
