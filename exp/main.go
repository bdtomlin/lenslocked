package main

import (
	"fmt"
	"lenslocked.com/models"
)

func main() {
	us, err := models.NewUserService()
	if err != nil {
		panic(err)
	}
	defer us.Close()
	us.DestructiveReset()

	user := models.User{
		Name:     "Michael Scott",
		Email:    "michael@dundermifflin.com",
		Password: "BestBoss",
	}
	err = us.Create(&user)
	if err != nil {
		panic(err)
	}

	fmt.Printf("%+v\n", user)
	if user.Remember == "" {
		panic("Invalid remember token")
	}

	user2, err := us.ByRemember(user.Remember)
	if err != nil {
		panic(err)
	}

	fmt.Printf("%+v\n", *user2)
}
